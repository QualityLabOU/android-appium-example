$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("qualitylab/feedback.feature");
formatter.feature({
  "line": 2,
  "name": "Send feedback",
  "description": "",
  "id": "send-feedback",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@feedback"
    }
  ]
});
formatter.before({
  "duration": 11006534417,
  "status": "passed"
});
formatter.before({
  "duration": 62896,
  "status": "passed"
});
formatter.before({
  "duration": 60850,
  "status": "passed"
});
formatter.before({
  "duration": 54791,
  "status": "passed"
});
formatter.before({
  "duration": 106178,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Send feedback to Kristiine keskus",
  "description": "",
  "id": "send-feedback;send-feedback-to-kristiine-keskus",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login with valid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should be logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I open the sidemenu",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I should see feedback button",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I click on feedback button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should be on the feedback activity",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I choose \"Kristiine Keskus\" from dropdown",
  "keyword": "When "
});
formatter.step({
  "line": 13,
  "name": "choose number \"2\" for feedback",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "press send feedback",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I should no see alert",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 1693334750,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 118618488,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_login_with_valid_credentials()"
});
formatter.result({
  "duration": 21074837430,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_be_logged_in()"
});
formatter.result({
  "duration": 1062289970,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_open_the_sidemenu()"
});
formatter.result({
  "duration": 197712584,
  "status": "passed"
});
formatter.match({
  "location": "FeedbackStepDefinitions.I_should_see_feedback_button()"
});
formatter.result({
  "duration": 740141223,
  "status": "passed"
});
formatter.match({
  "location": "FeedbackStepDefinitions.I_click_on_feedback_button()"
});
formatter.result({
  "duration": 589684026,
  "status": "passed"
});
formatter.match({
  "location": "FeedbackStepDefinitions.I_should_be_on_the_feedback_activity()"
});
formatter.result({
  "duration": 647636004,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Kristiine Keskus",
      "offset": 10
    }
  ],
  "location": "FeedbackStepDefinitions.I_choose_from_dropdown(String)"
});
formatter.result({
  "duration": 2074946272,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "2",
      "offset": 15
    }
  ],
  "location": "FeedbackStepDefinitions.choose_number_for_feedback(String)"
});
formatter.result({
  "duration": 2843851304,
  "status": "passed"
});
formatter.match({
  "location": "FeedbackStepDefinitions.press_send_feedback()"
});
formatter.result({
  "duration": 252825131,
  "status": "passed"
});
formatter.match({
  "location": "FeedbackStepDefinitions.I_should_no_see_alert()"
});
formatter.result({
  "duration": 910499078,
  "status": "passed"
});
formatter.after({
  "duration": 3591871351,
  "status": "passed"
});
formatter.uri("qualitylab/login.feature");
formatter.feature({
  "line": 2,
  "name": "Login to application",
  "description": "",
  "id": "login-to-application",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@login"
    }
  ]
});
formatter.before({
  "duration": 25170,
  "status": "passed"
});
formatter.before({
  "duration": 16823,
  "status": "passed"
});
formatter.before({
  "duration": 16383,
  "status": "passed"
});
formatter.before({
  "duration": 16694,
  "status": "passed"
});
formatter.before({
  "duration": 15670,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Login with invalid account",
  "description": "",
  "id": "login-to-application;login-with-invalid-account",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I login with invalid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I should see alert with text \"Vale kasutajanimi või salasõna.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 641366621,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_login_with_invalid_credentials()"
});
formatter.result({
  "duration": 18961172578,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Vale kasutajanimi või salasõna.",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3829848439,
  "status": "passed"
});
formatter.after({
  "duration": 250520457,
  "status": "passed"
});
formatter.uri("qualitylab/menu.feature");
formatter.feature({
  "line": 2,
  "name": "Menu actions",
  "description": "",
  "id": "menu-actions",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@menu"
    }
  ]
});
formatter.before({
  "duration": 34457,
  "status": "passed"
});
formatter.before({
  "duration": 19840,
  "status": "passed"
});
formatter.before({
  "duration": 15604,
  "status": "passed"
});
formatter.before({
  "duration": 14097,
  "status": "passed"
});
formatter.before({
  "duration": 17769,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Logout from sidemenu",
  "description": "",
  "id": "menu-actions;logout-from-sidemenu",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I login with valid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I should be logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I open the sidemenu",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I should see log out button",
  "keyword": "Then "
});
formatter.step({
  "line": 9,
  "name": "I log out",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "I am on the login page",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 135735162,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_login_with_valid_credentials()"
});
formatter.result({
  "duration": 28324823487,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_be_logged_in()"
});
formatter.result({
  "duration": 938832410,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_open_the_sidemenu()"
});
formatter.result({
  "duration": 207288365,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_see_log_out_button()"
});
formatter.result({
  "duration": 686179161,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_log_out()"
});
formatter.result({
  "duration": 632113066,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 617624219,
  "status": "passed"
});
formatter.after({
  "duration": 261751242,
  "status": "passed"
});
formatter.uri("qualitylab/register.feature");
formatter.feature({
  "line": 2,
  "name": ": Adding new user",
  "description": "",
  "id": ":-adding-new-user",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@register"
    }
  ]
});
formatter.before({
  "duration": 27416,
  "status": "passed"
});
formatter.before({
  "duration": 14826,
  "status": "passed"
});
formatter.before({
  "duration": 15113,
  "status": "passed"
});
formatter.before({
  "duration": 13535,
  "status": "passed"
});
formatter.before({
  "duration": 13939,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 97558424,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 417423150,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 575520269,
  "status": "passed"
});
formatter.scenario({
  "line": 9,
  "name": ": Add user with non-matching email addresses",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-non-matching-email-addresses",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 10,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I type \"marko@marko.com\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I type \"konsa@marko.com\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I type \"TestCase123\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I should see alert with text \"Sisestatud e-maili aadressid ei kattu!\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 8929104524,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 9430632803,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 19692311440,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 9676846110,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "konsa@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 9118292553,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10470661068,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 10041051626,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 334916439,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sisestatud e-maili aadressid ei kattu!",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3849676841,
  "status": "passed"
});
formatter.after({
  "duration": 771304714,
  "status": "passed"
});
formatter.before({
  "duration": 20910,
  "status": "passed"
});
formatter.before({
  "duration": 21744,
  "status": "passed"
});
formatter.before({
  "duration": 16455,
  "status": "passed"
});
formatter.before({
  "duration": 14827,
  "status": "passed"
});
formatter.before({
  "duration": 25102,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 516690072,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 469206376,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 616476093,
  "status": "passed"
});
formatter.scenario({
  "line": 20,
  "name": ": Add user with empty first name",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-empty-first-name",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 21,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 22,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I type \"marko@marko.com\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "I type \"marko@marko.com\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "I type \"TestCase123\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I should see alert with text \"Nimi peab olema vähemalt 2 tähemärki!\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 9138977322,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 20059260954,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 9515089545,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 8990407299,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10602000113,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 10015669578,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 501622269,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nimi peab olema vähemalt 2 tähemärki!",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3958441535,
  "status": "passed"
});
formatter.after({
  "duration": 599144904,
  "status": "passed"
});
formatter.before({
  "duration": 19089,
  "status": "passed"
});
formatter.before({
  "duration": 13972,
  "status": "passed"
});
formatter.before({
  "duration": 13931,
  "status": "passed"
});
formatter.before({
  "duration": 12585,
  "status": "passed"
});
formatter.before({
  "duration": 13148,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 514977822,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 429305290,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 640771966,
  "status": "passed"
});
formatter.scenario({
  "line": 30,
  "name": ": Add user with empty last name field",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-empty-last-name-field",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 31,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 33,
  "name": "I type \"marko@marko.com\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 34,
  "name": "I type \"konsa@marko.com\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 35,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "I type \"TestCase123\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 37,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 38,
  "name": "I should see alert with text \"Nimi peab olema vähemalt 2 tähemärki!\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 9169508796,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 19851816287,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 10112856473,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "konsa@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 9325716770,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10480251163,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 10505792210,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 319642442,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Nimi peab olema vähemalt 2 tähemärki!",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3866072426,
  "status": "passed"
});
formatter.after({
  "duration": 565071329,
  "status": "passed"
});
formatter.before({
  "duration": 18417,
  "status": "passed"
});
formatter.before({
  "duration": 12880,
  "status": "passed"
});
formatter.before({
  "duration": 13627,
  "status": "passed"
});
formatter.before({
  "duration": 12684,
  "status": "passed"
});
formatter.before({
  "duration": 12105,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 501481855,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 446785855,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 650837186,
  "status": "passed"
});
formatter.scenario({
  "line": 40,
  "name": ": Add user with invalid email address",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-invalid-email-address",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 41,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 42,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 44,
  "name": "I type \"markomarko\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 45,
  "name": "I type \"markomarko\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 46,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 47,
  "name": "I type \"TestCase123\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 48,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 49,
  "name": "I should see alert with text \"Sisestatud e-maili aadress on vigane!\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 8926725594,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 8963252792,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 20302659728,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "markomarko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 10056646937,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "markomarko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 8988069442,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10412312772,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 10577484264,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 298902923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sisestatud e-maili aadress on vigane!",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3820752861,
  "status": "passed"
});
formatter.after({
  "duration": 601050631,
  "status": "passed"
});
formatter.before({
  "duration": 18541,
  "status": "passed"
});
formatter.before({
  "duration": 14562,
  "status": "passed"
});
formatter.before({
  "duration": 14330,
  "status": "passed"
});
formatter.before({
  "duration": 12399,
  "status": "passed"
});
formatter.before({
  "duration": 12375,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 524199773,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 453203923,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 588152835,
  "status": "passed"
});
formatter.scenario({
  "line": 51,
  "name": ": Add user with empty birthday field",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-empty-birthday-field",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 52,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 53,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "I type \"marko@marko.com\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 55,
  "name": "I type \"marko@marko.com\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 56,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 57,
  "name": "I type \"TestCase123\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 58,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 59,
  "name": "I should see alert with text \"Sünnipäeva väli on tühi!\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 8913125378,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 8983791232,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 9203433431,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 9157764477,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10395390686,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 10002262201,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 271777287,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Sünnipäeva väli on tühi!",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3911363035,
  "status": "passed"
});
formatter.after({
  "duration": 616146751,
  "status": "passed"
});
formatter.before({
  "duration": 19503,
  "status": "passed"
});
formatter.before({
  "duration": 14435,
  "status": "passed"
});
formatter.before({
  "duration": 12992,
  "status": "passed"
});
formatter.before({
  "duration": 12147,
  "status": "passed"
});
formatter.before({
  "duration": 12292,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 555224629,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 479356581,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 601577817,
  "status": "passed"
});
formatter.scenario({
  "line": 61,
  "name": ": Add user with empty passwords",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-empty-passwords",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 62,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 63,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I type \"marko@marko.com\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 66,
  "name": "I type \"marko@marko.com\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 67,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 68,
  "name": "I should see alert with text \"Salasõna peab sisaldama\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 8924779070,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 9005348709,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 19610794684,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 9671255959,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 9183020799,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 376334923,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Salasõna peab sisaldama",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3847118063,
  "status": "passed"
});
formatter.after({
  "duration": 584747838,
  "status": "passed"
});
formatter.before({
  "duration": 25190,
  "status": "passed"
});
formatter.before({
  "duration": 15684,
  "status": "passed"
});
formatter.before({
  "duration": 14902,
  "status": "passed"
});
formatter.before({
  "duration": 12983,
  "status": "passed"
});
formatter.before({
  "duration": 12760,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 515764150,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 486551433,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 601288598,
  "status": "passed"
});
formatter.scenario({
  "line": 70,
  "name": ": Add user with non-matching passwords",
  "description": "",
  "id": ":-adding-new-user;:-add-user-with-non-matching-passwords",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 71,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 72,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "I type \"marko@marko.com\" into the email",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": "I type \"marko@marko.com\" into the email again",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 77,
  "name": "I type \"TestCase124\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 78,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 79,
  "name": "I should see alert with text \"Uued paroolid ei kattu\"",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 9045578839,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 8866257304,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 19825796082,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email(String)"
});
formatter.result({
  "duration": 9668283617,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "marko@marko.com",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_email_again(String)"
});
formatter.result({
  "duration": 9235362398,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10201944986,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase124",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 9157553947,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 322717112,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Uued paroolid ei kattu",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3828749900,
  "status": "passed"
});
formatter.after({
  "duration": 660268703,
  "status": "passed"
});
formatter.before({
  "duration": 22407,
  "status": "passed"
});
formatter.before({
  "duration": 12465,
  "status": "passed"
});
formatter.before({
  "duration": 12867,
  "status": "passed"
});
formatter.before({
  "duration": 12137,
  "status": "passed"
});
formatter.before({
  "duration": 13522,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 510450034,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 513988784,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 593542407,
  "status": "passed"
});
formatter.scenario({
  "line": 81,
  "name": "Add user with correct credentials",
  "description": "",
  "id": ":-adding-new-user;add-user-with-correct-credentials",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 82,
  "name": "I type \"Marko\" into the first name",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "I type \"Konsa\" into the last name",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "I enter the birthday",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "I enter unique email address",
  "keyword": "And "
});
formatter.step({
  "line": 86,
  "name": "I repeat unique email address",
  "keyword": "And "
});
formatter.step({
  "line": 87,
  "name": "I type \"TestCase123\" into the password",
  "keyword": "And "
});
formatter.step({
  "line": 88,
  "name": "I type \"TestCase123\" into the password again",
  "keyword": "And "
});
formatter.step({
  "line": 89,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 90,
  "name": "I am on the login page",
  "keyword": "Then "
});
formatter.step({
  "line": 91,
  "name": "save lastly created user",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "Marko",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_first_name(String)"
});
formatter.result({
  "duration": 9484271787,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konsa",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_last_name(String)"
});
formatter.result({
  "duration": 9069946104,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_set_the_birthday()"
});
formatter.result({
  "duration": 19928484403,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_unique_email_address()"
});
formatter.result({
  "duration": 11685013353,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.I_repeat_unique_email_address()"
});
formatter.result({
  "duration": 11050155640,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password(String)"
});
formatter.result({
  "duration": 10087935560,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase123",
      "offset": 8
    }
  ],
  "location": "RegisterStepDefinitions.i_type_into_the_password_again(String)"
});
formatter.result({
  "duration": 10570815919,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 281649903,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 1987237813,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.save_lastly_created_user_email()"
});
formatter.result({
  "duration": 2312233,
  "status": "passed"
});
formatter.after({
  "duration": 296178086,
  "status": "passed"
});
formatter.before({
  "duration": 22320,
  "status": "passed"
});
formatter.before({
  "duration": 12535,
  "status": "passed"
});
formatter.before({
  "duration": 14340,
  "status": "passed"
});
formatter.before({
  "duration": 11970,
  "status": "passed"
});
formatter.before({
  "duration": 11998,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "Navigate to register activity",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I navigate to registering activity",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should see register button",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 118544588,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_navigate_to_registering_activity()"
});
formatter.result({
  "duration": 409835709,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_should_see_register_button()"
});
formatter.result({
  "duration": 642072095,
  "status": "passed"
});
formatter.scenario({
  "line": 93,
  "name": "Add user with already registered email",
  "description": "",
  "id": ":-adding-new-user;add-user-with-already-registered-email",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 94,
  "name": "I enter valid account details to registering form",
  "keyword": "When "
});
formatter.step({
  "line": 95,
  "name": "I press register button",
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "I should see alert with text \"Konto juba olemas.\"",
  "keyword": "Then "
});
formatter.match({
  "location": "RegisterStepDefinitions.I_enter_valid_account_details_to_registering_form()"
});
formatter.result({
  "duration": 79465340721,
  "status": "passed"
});
formatter.match({
  "location": "RegisterStepDefinitions.i_press_register_button()"
});
formatter.result({
  "duration": 228163912,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Konto juba olemas.",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 3996392239,
  "status": "passed"
});
formatter.after({
  "duration": 684189178,
  "status": "passed"
});
formatter.uri("qualitylab/settings.feature");
formatter.feature({
  "line": 2,
  "name": "Actions inside settings",
  "description": "",
  "id": "actions-inside-settings",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@settings"
    }
  ]
});
formatter.before({
  "duration": 17904,
  "status": "passed"
});
formatter.before({
  "duration": 12694,
  "status": "passed"
});
formatter.before({
  "duration": 12284,
  "status": "passed"
});
formatter.before({
  "duration": 13328,
  "status": "passed"
});
formatter.before({
  "duration": 11862,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Check account details",
  "description": "",
  "id": "actions-inside-settings;check-account-details",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 5,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I login with valid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "I should be logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 8,
  "name": "I open the sidemenu",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I should see settings button",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "I click settings button",
  "keyword": "When "
});
formatter.step({
  "line": 11,
  "name": "I should be on the my settings activity",
  "keyword": "Then "
});
formatter.step({
  "line": 12,
  "name": "I check account details",
  "keyword": "And "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 512291258,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_login_with_valid_credentials()"
});
formatter.result({
  "duration": 21817231402,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_be_logged_in()"
});
formatter.result({
  "duration": 932512092,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_open_the_sidemenu()"
});
formatter.result({
  "duration": 242689073,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_see_log_out_button()"
});
formatter.result({
  "duration": 663805104,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_click_settings_button()"
});
formatter.result({
  "duration": 548089474,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_be_on_the_my_settings_activity()"
});
formatter.result({
  "duration": 627108608,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_check_account_details()"
});
formatter.result({
  "duration": 615388491,
  "status": "passed"
});
formatter.after({
  "duration": 3845186949,
  "status": "passed"
});
formatter.before({
  "duration": 23123,
  "status": "passed"
});
formatter.before({
  "duration": 15289,
  "status": "passed"
});
formatter.before({
  "duration": 18216,
  "status": "passed"
});
formatter.before({
  "duration": 12870,
  "status": "passed"
});
formatter.before({
  "duration": 12579,
  "status": "passed"
});
formatter.scenario({
  "line": 15,
  "name": "Change password and try to log in with old one and check",
  "description": "",
  "id": "actions-inside-settings;change-password-and-try-to-log-in-with-old-one-and-check",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 14,
      "name": "@validAddress"
    }
  ]
});
formatter.step({
  "line": 16,
  "name": "I am on the login page",
  "keyword": "Given "
});
formatter.step({
  "line": 17,
  "name": "I login with valid credentials",
  "keyword": "When "
});
formatter.step({
  "line": 18,
  "name": "I should be logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 19,
  "name": "I open the sidemenu",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I should see settings button",
  "keyword": "Then "
});
formatter.step({
  "line": 21,
  "name": "I click settings button",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "I should be on the my settings activity",
  "keyword": "Then "
});
formatter.step({
  "line": 23,
  "name": "I change my password to \"TestCase111\"",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "I should be on the my settings activity",
  "keyword": "Then "
});
formatter.step({
  "line": 25,
  "name": "I open the sidemenu",
  "keyword": "When "
});
formatter.step({
  "line": 26,
  "name": "I should see log out button",
  "keyword": "Then "
});
formatter.step({
  "line": 27,
  "name": "I log out",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "I am on the login page",
  "keyword": "Then "
});
formatter.step({
  "line": 29,
  "name": "I try to log in with old password",
  "keyword": "When "
});
formatter.step({
  "line": 30,
  "name": "I should see alert with text \"Vale kasutajanimi või salasõna.\"",
  "keyword": "Then "
});
formatter.step({
  "line": 31,
  "name": "I login with valid email and \"TestCase111\"",
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "I should be logged in",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "I open the sidemenu",
  "keyword": "When "
});
formatter.step({
  "line": 34,
  "name": "I should see settings button",
  "keyword": "Then "
});
formatter.step({
  "line": 35,
  "name": "I click settings button",
  "keyword": "When "
});
formatter.step({
  "line": 36,
  "name": "I should be on the my settings activity",
  "keyword": "Then "
});
formatter.step({
  "line": 37,
  "name": "I check account details",
  "keyword": "And "
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 633493546,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_login_with_valid_credentials()"
});
formatter.result({
  "duration": 21568238321,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_be_logged_in()"
});
formatter.result({
  "duration": 946936832,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_open_the_sidemenu()"
});
formatter.result({
  "duration": 239799345,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_see_log_out_button()"
});
formatter.result({
  "duration": 777080899,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_click_settings_button()"
});
formatter.result({
  "duration": 481415674,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_be_on_the_my_settings_activity()"
});
formatter.result({
  "duration": 635915370,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase111",
      "offset": 25
    }
  ],
  "location": "SettingsStepDefinitions.I_change_my_password_to(String)"
});
formatter.result({
  "duration": 19521132465,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_be_on_the_my_settings_activity()"
});
formatter.result({
  "duration": 747653930,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_open_the_sidemenu()"
});
formatter.result({
  "duration": 175176998,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_see_log_out_button()"
});
formatter.result({
  "duration": 689933606,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_log_out()"
});
formatter.result({
  "duration": 610591622,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.i_am_on_the_login_page()"
});
formatter.result({
  "duration": 613067272,
  "status": "passed"
});
formatter.match({
  "location": "LoginStepDefinitions.I_try_to_log_in_with_old_password()"
});
formatter.result({
  "duration": 20326516037,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Vale kasutajanimi või salasõna.",
      "offset": 30
    }
  ],
  "location": "RegisterStepDefinitions.I_should_see_alert_with_text(String)"
});
formatter.result({
  "duration": 4036947794,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TestCase111",
      "offset": 30
    }
  ],
  "location": "LoginStepDefinitions.I_login_with_valid_email_and(String)"
});
formatter.result({
  "duration": 25911468515,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_should_be_logged_in()"
});
formatter.result({
  "duration": 2315305408,
  "status": "passed"
});
formatter.match({
  "location": "MenuActivityStepDefinitions.I_open_the_sidemenu()"
});
formatter.result({
  "duration": 202279341,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_see_log_out_button()"
});
formatter.result({
  "duration": 715408335,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_click_settings_button()"
});
formatter.result({
  "duration": 657723209,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_should_be_on_the_my_settings_activity()"
});
formatter.result({
  "duration": 661791072,
  "status": "passed"
});
formatter.match({
  "location": "SettingsStepDefinitions.I_check_account_details()"
});
formatter.result({
  "duration": 646787211,
  "status": "passed"
});
formatter.after({
  "duration": 3998191553,
  "status": "passed"
});
formatter.after({
  "duration": 83066662323,
  "status": "passed"
});
});