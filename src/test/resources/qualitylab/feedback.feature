@feedback
Feature: Send feedback
  Scenario: Send feedback to Kristiine keskus
    Given I am on the login page
    Given I am on the login page
    When I login with valid credentials
    Then I should be logged in
    When I open the sidemenu
    Then I should see feedback button
    When I click on feedback button
    Then I should be on the feedback activity
    When I choose "Kristiine Keskus" from dropdown
    And choose number "2" for feedback
    And press send feedback
    Then I should no see alert