@login
Feature: Login to application
  Scenario: Login with invalid account
    Given I am on the login page
    When I login with invalid credentials
    Then I should see alert with text "Vale kasutajanimi või salasõna."

