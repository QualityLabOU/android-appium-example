@menu
Feature: Menu actions
  Scenario: Logout from sidemenu
    Given I am on the login page
    When I login with valid credentials
    Then I should be logged in
    When I open the sidemenu
    Then I should see log out button
    When I log out
    Then I am on the login page
