@register
Feature: : Adding new user

  Background: Navigate to register activity
    Given I am on the login page
    When I navigate to registering activity
    Then I should see register button

  Scenario: : Add user with non-matching email addresses
    When I type "Marko" into the first name
    And I type "Konsa" into the last name
    And I enter the birthday
    And I type "marko@marko.com" into the email
    And I type "konsa@marko.com" into the email again
    And I type "TestCase123" into the password
    And I type "TestCase123" into the password again
    And I press register button
    Then I should see alert with text "Sisestatud e-maili aadressid ei kattu!"

  Scenario: : Add user with empty first name
    And I type "Konsa" into the last name
    And I enter the birthday
    And I type "marko@marko.com" into the email
    And I type "marko@marko.com" into the email again
    And I type "TestCase123" into the password
    And I type "TestCase123" into the password again
    And I press register button
    Then I should see alert with text "Nimi peab olema vähemalt 2 tähemärki!"

  Scenario: : Add user with empty last name field
    When I type "Marko" into the first name
    And I enter the birthday
    And I type "marko@marko.com" into the email
    And I type "konsa@marko.com" into the email again
    And I type "TestCase123" into the password
    And I type "TestCase123" into the password again
    And I press register button
    Then I should see alert with text "Nimi peab olema vähemalt 2 tähemärki!"

  Scenario: : Add user with invalid email address
    When I type "Marko" into the first name
    And I type "Konsa" into the last name
    And I enter the birthday
    And I type "markomarko" into the email
    And I type "markomarko" into the email again
    And I type "TestCase123" into the password
    And I type "TestCase123" into the password again
    And I press register button
    Then I should see alert with text "Sisestatud e-maili aadress on vigane!"

  Scenario: : Add user with empty birthday field
    When I type "Marko" into the first name
    And I type "Konsa" into the last name
    And I type "marko@marko.com" into the email
    And I type "marko@marko.com" into the email again
    And I type "TestCase123" into the password
    And I type "TestCase123" into the password again
    And I press register button
    Then I should see alert with text "Sünnipäeva väli on tühi!"

  Scenario: : Add user with empty passwords
    When I type "Marko" into the first name
    And I type "Konsa" into the last name
    And I enter the birthday
    And I type "marko@marko.com" into the email
    And I type "marko@marko.com" into the email again
    And I press register button
    Then I should see alert with text "Salasõna peab sisaldama"

  Scenario: : Add user with non-matching passwords
    When I type "Marko" into the first name
    And I type "Konsa" into the last name
    And I enter the birthday
    And I type "marko@marko.com" into the email
    And I type "marko@marko.com" into the email again
    And I type "TestCase123" into the password
    And I type "TestCase124" into the password again
    And I press register button
    Then I should see alert with text "Uued paroolid ei kattu"

  Scenario: Add user with correct credentials
    When I type "Marko" into the first name
    And I type "Konsa" into the last name
    And I enter the birthday
    And I enter unique email address
    And I repeat unique email address
    And I type "TestCase123" into the password
    And I type "TestCase123" into the password again
    And I press register button
    Then I am on the login page
    And save lastly created user

  Scenario: Add user with already registered email
    When I enter valid account details to registering form
    And I press register button
    Then I should see alert with text "Konto juba olemas."