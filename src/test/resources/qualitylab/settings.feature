@settings
Feature: Actions inside settings

  Scenario: Check account details
    Given I am on the login page
    When I login with valid credentials
    Then I should be logged in
    When I open the sidemenu
    Then I should see settings button
    When I click settings button
    Then I should be on the my settings activity
    And I check account details

  @validAddress
  Scenario: Change password and try to log in with old one and check
    Given I am on the login page
    When I login with valid credentials
    Then I should be logged in
    When I open the sidemenu
    Then I should see settings button
    When I click settings button
    Then I should be on the my settings activity
    And I change my password to "TestCase111"
    Then I should be on the my settings activity
    When I open the sidemenu
    Then I should see log out button
    When I log out
    Then I am on the login page
    When I try to log in with old password
    Then I should see alert with text "Vale kasutajanimi või salasõna."
    When I login with valid email and "TestCase111"
    Then I should be logged in
    When I open the sidemenu
    Then I should see settings button
    When I click settings button
    Then I should be on the my settings activity
    And I check account details
