package qualitylab.helpers;

import qualitylab.step_definitions.AbstractStepDefinitions;
import org.openqa.selenium.WebElement;

public class InputHelper extends AbstractStepDefinitions{

  public static void setInputValue(WebElement element, String value, String error) {
    if (element.isDisplayed()){
      element.clear();
      element.sendKeys(value);
      driver.hideKeyboard();
    } else {
      throw new AssertionError(error);
    }
  }

}
