package qualitylab.helpers;

import qualitylab.step_definitions.AbstractStepDefinitions;
import org.openqa.selenium.WebElement;

import java.util.List;

public class AlertHelper extends AbstractStepDefinitions {

  public static void shouldHaveAlert(String text, String error) {

    for (WebElement element : driver.findElementsByClassName("android.widget.TextView")) {
      if (element.getText().contains(text)) {
        driver.findElementByName("OK").click();
        return;
      }
    }
    throw new AssertionError(error);
  }

  public static boolean isAlert() {
    for (WebElement element : driver.findElementsByClassName("android.widget.TextView")) {
      if (element.getText().contains("Viga")) {
        return true;
      }
    }
    return false;
  }

  public static String getAlertError() {

    List<WebElement> textViews = driver.findElementsByClassName("android.widget.TextView");

    for (int i = 0; i < textViews.size(); i++) {
      if (textViews.get(i).getText().contains("Viga")) {
        return textViews.get(i + 1).getText();
      }
    }
    throw new AssertionError("Something went wrong, There is no alert?");
  }

  public static void dismissAlert() {
    driver.findElementByName("OK").click();
  }
}
