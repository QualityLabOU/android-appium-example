package qualitylab.utility;

public class Account {

  String firstname;
  String lastname;
  String birthday;
  String email;
  String emailVerification;
  String password;
  String passwordVerifivation;

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  public String getBirthday() {
    return birthday;
  }

  public void setBirthday(String birthday) {
    this.birthday = birthday;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEmailVerification() {
    return emailVerification;
  }

  public void setEmailVerification(String emailVerification) {
    this.emailVerification = emailVerification;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getPasswordVerifivation() {
    return passwordVerifivation;
  }

  public void setPasswordVerifivation(String passwordVerifivation) {
    this.passwordVerifivation = passwordVerifivation;
  }
}
