package qualitylab.utility;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class JSONFileHandler {

  public static final String path = "src/test/resources/qualitylab/utility/last.txt";
  public static final String FIRSTNAME_TAG = "Firstname";
  public static final String LASTNAME_TAG = "Lastname";
  public static final String BIRTHDAY_TAG = "Birthday";
  public static final String EMAIL_TAG = "Email";
  public static final String EMAIL_VERIFICATION_TAG = "EmailVerification";
  public static final String PASSWORD_TAG = "Password";
  public static final String PASSWORD_VERIFICATION_TAG = "PasswordVerification";


  @SuppressWarnings("unchecked")
  public void saveToFile(Account account) throws IOException {

    FileWriter file = new FileWriter(path);
    try {
      JSONObject obj = new JSONObject();
      obj.put(FIRSTNAME_TAG, account.getFirstname());
      obj.put(LASTNAME_TAG, account.getLastname());
      obj.put(EMAIL_TAG, account.getEmail());
      obj.put(EMAIL_VERIFICATION_TAG, account.getEmailVerification());
      obj.put(BIRTHDAY_TAG, account.getBirthday());
      obj.put(PASSWORD_TAG, account.getPassword());
      obj.put(PASSWORD_VERIFICATION_TAG, account.getPasswordVerifivation());

      file.write(obj.toJSONString());
      System.out.println("Successfully Copied JSON Object to File...");
      System.out.println("\nJSON Object: " + obj);

    } finally {
      file.flush();
      file.close();
    }
  }

  public Account loadAccountFromJson() throws IOException, ParseException {

    JSONParser parser = new JSONParser();

    Object obj = parser.parse(new FileReader(path));
    return parseAccount((JSONObject) obj);
  }

  private Account parseAccount(JSONObject jsonObject) {
    Account account = new Account();
    account.setFirstname((String) jsonObject.get(FIRSTNAME_TAG));
    account.setLastname((String) jsonObject.get(LASTNAME_TAG));
    account.setBirthday((String) jsonObject.get(BIRTHDAY_TAG));
    account.setEmail((String) jsonObject.get(EMAIL_TAG));
    account.setEmailVerification((String) jsonObject.get(EMAIL_VERIFICATION_TAG));
    account.setPassword((String) jsonObject.get(PASSWORD_TAG));
    account.setPasswordVerifivation((String) jsonObject.get(PASSWORD_VERIFICATION_TAG));
    return account;
  }

}
