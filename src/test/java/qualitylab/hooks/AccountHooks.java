package qualitylab.hooks;

import qualitylab.activities.LoginActivity;
import qualitylab.activities.RegisterActivity;
import qualitylab.step_definitions.AbstractStepDefinitions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.json.simple.parser.ParseException;

import java.io.IOException;

public class AccountHooks extends AbstractStepDefinitions {

  @After("@validAddress")
  public void registerValidAddress(Scenario scenario) throws IOException, ParseException {

    LoginActivity loginActivity = new LoginActivity(scenario);

    if (loginActivity.isLoginScreen()){
      startRegistreringFromLoginScreen(scenario);
    }else {
      LogoutHandler logoutHandler = new LogoutHandler();
      logoutHandler.navigateToLoginPage(scenario);
      startRegistreringFromLoginScreen(scenario);
    }
  }

  private void startRegistreringFromLoginScreen(Scenario scenario) throws IOException, ParseException {
    RegisterActivity registerActivity = new RegisterActivity(scenario);
    LoginActivity loginActivity = new LoginActivity(scenario);
    loginActivity.clickRegisterNewUser();
    registerActivity.registerWithValidInformation();
    registerActivity.pressRegister();
    registerActivity.saveAccount();
  }
}
