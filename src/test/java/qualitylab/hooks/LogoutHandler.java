package qualitylab.hooks;

import qualitylab.activities.MenuActivity;
import qualitylab.helpers.AlertHelper;
import qualitylab.step_definitions.AbstractStepDefinitions;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import org.openqa.selenium.WebElement;

public class LogoutHandler extends AbstractStepDefinitions {

  @After
  public void navigateToLoginPage(Scenario scenario) {

    if (AlertHelper.isAlert()) {
      driver.findElementByName("OK").click();
    }

    if (driver.currentActivity().equals(".LoginActivity") ||
            driver.currentActivity().equals(".RegistrationActivity") ||
            driver.currentActivity().equals(".ExistingUserRegistrationActivity") ||
            driver.currentActivity().equals(".PasswordRecoveryActivity")) {

      while (!driver.currentActivity().equals(".LoginActivity")){
        driver.navigate().back();
      }

    } else if (driver.currentActivity().equals(".MenuActivity")) {
      searchForLogOutButton();

      MenuActivity menuActivity = new MenuActivity(scenario);
      menuActivity.openSideMenu();
      searchForLogOutButton();
    }
    }

  private void searchForLogOutButton() {
    for (WebElement element : driver.findElementsByClassName("android.widget.TextView")) {
      if (element.getText().contains("Logi välja")) {
        element.click();
        return;
      }
    }
  }
}
