package qualitylab;

import qualitylab.step_definitions.AbstractStepDefinitions;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.net.MalformedURLException;

@RunWith(Cucumber.class)
@CucumberOptions(
        format = {"pretty", "html:reports/html"},
        tags = {"@menu, @settings, @login, @register, @feedback"} //what tags to incluse(@)/exclude(@~)
)
public class AppiumTest extends AbstractStepDefinitions {


  @BeforeClass
  public static void setup() throws MalformedURLException {
    System.out.print("Tests started");
  }

  @AfterClass
  public static void tearDown() {
    System.out.print("Tests ended");
  }

}