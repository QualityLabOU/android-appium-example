package qualitylab.activities;

import qualitylab.helpers.AlertHelper;
import qualitylab.step_definitions.AbstractStepDefinitions;
import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.MalformedURLException;

public class FeedbackActivity extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;

  public FeedbackActivity(Scenario scenario) {
    this.scenario = scenario;
    try {
      driver = getAndroidDriver();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private WebElement getFeedbackButton(){
    return driver.findElementByName("Tagasiside");
  }

  private WebElement getSendFeedbackButton(){
    return driver.findElement(By.id("com.:id/sendFeedbackButton"));
  }

  private WebElement getDecreaseButton(){
    return driver.findElement(By.id("com.qualitylab:id/decreaseButton"));
  }

  private WebElement getIncreaseButton() {
    return driver.findElement(By.id("com.qualitylab:id/increaseButton"));
  }

  private WebElement getNumberTextView(){
    return driver.findElement(By.id("com.qualitylab:id/numberTextView"));
  }

  private WebElement getPlacesButton() {
    return driver.findElement(By.id("com.qualitylab:id/placesButton"));
  }

  public void choosePlace(String value){
    driver.findElementByName(value).click();
  }

  public void shouldSeeFeedbackButton() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[contains(@text, 'Tagasiside')]")));
  }

  public void clickOnFeedbackButton() {
    getFeedbackButton().click();
  }

  public void shouldBeOnTheFeedbackActivity() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.Button[contains(@text, 'Saada tagasiside')]")));
  }

  public void chooseFromDropDown(String place) {
    getPlacesButton().click();
    choosePlace(place);
  }

  public void chooseFeedbackNumber(String feedback) {
    while(!getNumberTextView().getText().equals(feedback)){
      if (Integer.parseInt(getNumberTextView().getText()) > Integer.parseInt(feedback)){
        getDecreaseButton().click();
      }else if (Integer.parseInt(getNumberTextView().getText()) < Integer.parseInt(feedback)){
        getIncreaseButton().click();
      }
    }
  }

  public void pressSendFeedback() {
    getSendFeedbackButton().click();
  }

  public void shouldNotSeeAlert() {
    if (AlertHelper.isAlert()){
      throw new AssertionError("Alert found: "+AlertHelper.getAlertError());
    }
  }
}
