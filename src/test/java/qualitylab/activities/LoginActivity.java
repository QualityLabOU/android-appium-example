package qualitylab.activities;

import qualitylab.helpers.AlertHelper;
import qualitylab.helpers.InputHelper;
import qualitylab.step_definitions.AbstractStepDefinitions;
import qualitylab.utility.Account;
import qualitylab.utility.JSONFileHandler;
import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.net.MalformedURLException;

public class LoginActivity extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;

  public LoginActivity(Scenario scenario) {
    this.scenario = scenario;
    try {
      driver = getAndroidDriver();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private WebElement getLoginButton() {
    return driver.findElement(By.id("com.qualitylab:id/loginButton"));
  }

  private WebElement getRegisterNewUser() {
    wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("com.qualitylab:id/register"))));
    return driver.findElement(By.id("com.qualitylab:id/register"));
  }

  private WebElement getEmailInput() {
    return driver.findElement(By.id("com.qualitylab:id/email"));
  }

  private WebElement getPasswordInput() {
    return driver.findElement(By.id("com.qualitylab:id/password"));
  }

  public void shoudSeeLoginButton() {
    wait.until(ExpectedConditions.elementToBeClickable(By
            .xpath("//android.widget.Button[contains(@text, 'Logi sisse')]")));
  }

  public boolean isLoginScreen(){
    for (WebElement element : driver.findElementsByClassName("android.widget.Button")){
      if (element.getText().contains("Logi sisse")) return true;
    }
    return false;
  }

  public void clickRegisterNewUser() {
   getRegisterNewUser().click();
  }

  public void loginWithValidCredentials() throws IOException, ParseException {
    JSONFileHandler jsonFileHandler = new JSONFileHandler();
    Account account = jsonFileHandler.loadAccountFromJson();
    InputHelper.setInputValue(getEmailInput(), account.getEmail(), "Entering email failed");
    InputHelper.setInputValue(getPasswordInput(), account.getPassword(),"Entering password failed");
    getLoginButton().click();
    checkLogin(jsonFileHandler);
  }

  public void loginWithInvalidCredentials() {
    InputHelper.setInputValue(getEmailInput(), "InvalidEmailAddress@invalid.bra", "Entering email failed");
    InputHelper.setInputValue(getPasswordInput(), "InvalidPassword123","Entering password failed");
    getLoginButton().click();
  }

  public void loginWithValidEmailAnd(String password) throws IOException, ParseException {
    JSONFileHandler jsonFileHandler = new JSONFileHandler();
    Account account = jsonFileHandler.loadAccountFromJson();
    InputHelper.setInputValue(getEmailInput(), account.getEmail(), "Entering email failed");
    InputHelper.setInputValue(getPasswordInput(), password,"Entering password failed");
    getLoginButton().click();
  }

  public void loginWithOldPassword() throws IOException, ParseException {
    JSONFileHandler jsonFileHandler = new JSONFileHandler();
    Account account = jsonFileHandler.loadAccountFromJson();
    InputHelper.setInputValue(getEmailInput(), account.getEmail(), "Entering email failed");
    InputHelper.setInputValue(getPasswordInput(), account.getPassword(),"Entering password failed");
    getLoginButton().click();
  }

  private void checkLogin(JSONFileHandler jsonFileHandler) throws IOException, ParseException {
    if  (AlertHelper.isAlert()){
      scenario.write(AlertHelper.getAlertError());
      scenario.write("Registering new valid account...");
      AlertHelper.dismissAlert();
      getRegisterNewUser().click();
      RegisterActivity registerActivity = new RegisterActivity(scenario);
      registerActivity.registerWithValidInformation();
      registerActivity.pressRegister();
      registerActivity.saveAccount();
      scenario.write("Account is registered...Try again to login now.");

      shoudSeeLoginButton();
      Account newAccount = jsonFileHandler.loadAccountFromJson();
      InputHelper.setInputValue(getEmailInput(), newAccount.getEmail(), "Entering email failed");
      InputHelper.setInputValue(getPasswordInput(), newAccount.getPassword(),"Entering password failed");
      getLoginButton().click();


    }
  }
}
