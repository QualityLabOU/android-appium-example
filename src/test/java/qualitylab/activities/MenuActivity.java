package qualitylab.activities;

import qualitylab.step_definitions.AbstractStepDefinitions;
import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.net.MalformedURLException;

public class MenuActivity extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;

  public MenuActivity(Scenario scenario) {
    this.scenario = scenario;
    try {
      driver = getAndroidDriver();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private WebElement getSideMenuButton(){
    return driver.findElement(By.id("android:id/home"));
  }

  private WebElement getLogoutButton(){
    return driver.findElement(By.xpath("//android.widget.RelativeLayout[1]/android.view.View[2]/android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.ListView[1]/android.widget.RelativeLayout[9]"));
  }

  public void shouldBeLoggedIn() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[contains(@text, 'Minu profiil')]")));
  }

  public void openSideMenu() {
    getSideMenuButton().click();
  }

  public void shoudSeeLogoutButton() {
    getLogoutButton().isDisplayed();
  }

  public void pressLogout() {
    getLogoutButton().click();
  }

}
