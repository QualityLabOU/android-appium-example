package qualitylab.activities;

import qualitylab.helpers.InputHelper;
import qualitylab.step_definitions.AbstractStepDefinitions;
import qualitylab.utility.Account;
import qualitylab.utility.JSONFileHandler;
import qualitylab.utility.ParsedObject;
import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;

  public SettingsActivity(Scenario scenario) {
    this.scenario = scenario;
    try {
      driver = getAndroidDriver();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private WebElement getSettingsButton() {
    return driver.findElementByName("Minu seaded");
  }

  private WebElement getPasswordInput() {
    return driver.findElement(By.id("com.qualitylab:id/password"));
  }

  private WebElement getRetypePasswordInput() {
    return driver.findElement(By.id("com.qualitylab:id/retype_password"));
  }

  public void shouldSeeSettingsButton() {
    getSettingsButton().isDisplayed();
  }

  public void clickSettingsButton() {
    getSettingsButton().click();
  }

  public void shouldBeOnTheSettingsActivity() {
    wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//android.widget.TextView[contains(@text, 'Minu seaded')]")));

  }

  private List<ParsedObject> getAccountDetailsFromSettings() {
    List<ParsedObject> parsedObjects = new ArrayList<ParsedObject>();
    for (WebElement element : driver.findElementsByClassName("android.widget.TextView")) {
      if (element.getText().contains(": ")) {
        String[] array = element.getText().split(": ");
        ParsedObject parsedObject = new ParsedObject();
        parsedObject.setKey(array[0]);
        try {
          parsedObject.setValue(array[1]);
        } catch (ArrayIndexOutOfBoundsException e) {
          //It's okay
        }
        parsedObjects.add(parsedObject);
      }
    }
    return parsedObjects;
  }

  public void checkAccountDetails() throws IOException, ParseException {
    JSONFileHandler handler = new JSONFileHandler();
    Account validAccount = handler.loadAccountFromJson();

    List<ParsedObject> parsedObjects = getAccountDetailsFromSettings();
    for (ParsedObject object : parsedObjects) {
      if (object.getKey().equals("Nimi") && !object.getValue().equals(validAccount.getFirstname() + " " + validAccount.getLastname())) {
        throw new AssertionError("Expected: " + object.getValue() + " Actual: " + validAccount.getFirstname() + " " + validAccount.getLastname());
      } else if (object.getKey().equals("Sünnipäev") && !object.getValue().equals(validAccount.getBirthday())) {
        throw new AssertionError("Expected: " + object.getValue() + " Actual: " + validAccount.getBirthday());
      } else if (object.getKey().equals("Emails") && !object.getValue().equals(validAccount.getEmail())) {
        throw new AssertionError("Expected: " + object.getValue() + " Actual: " + validAccount.getEmail());
      }
    }
  }

  public void changePasswordTo(String password) {

    InputHelper.setInputValue(getPasswordInput(), password, "Password typing went wrong");
    InputHelper.setInputValue(getRetypePasswordInput(), password, "Retype password typing went wrong");
    driver.findElementByName("Salvesta").click();
  }
}