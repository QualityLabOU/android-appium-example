package qualitylab.activities;

import qualitylab.helpers.AlertHelper;
import qualitylab.helpers.InputHelper;
import qualitylab.step_definitions.AbstractStepDefinitions;
import qualitylab.utility.Account;
import qualitylab.utility.JSONFileHandler;
import cucumber.api.Scenario;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.security.SecureRandom;
import java.util.List;

public class RegisterActivity extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;
  public static Account account;

  public RegisterActivity(Scenario scenario) {
    this.scenario = scenario;
    account = new Account();
    try {
      driver = getAndroidDriver();
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private WebElement getRegisterButton() {
    return driver.findElement(By.id("com.qualitylab:id/registerButton_registration"));
  }

  private WebElement getFirstNameInput() {
    return driver.findElement(By.id("com.qualitylab:id/first_name_registration"));
  }

  private WebElement getLastNameInput() {
    return driver.findElement(By.id("com.qualitylab:id/last_name_registration"));
  }

  private WebElement getBirthDayInput() {
    return driver.findElement(By.id("com.qualitylab:id/birthday_registration"));
  }

  private WebElement getFirstEmailInput() {
    return driver.findElement(By.id("com.qualitylab:id/email_registration"));
  }

  private WebElement getSecondEmailInput() {
    return driver.findElement(By.id("com.qualitylab:id/email_verification_registration"));
  }

  private WebElement getFirstPasswordInput() {
    return driver.findElement(By.id("com.qualitylab:id/password_registration"));
  }

  private WebElement getSecondPasswordInput() {
    return driver.findElement(By.id("com.qualitylab:id/password_verification_registration"));
  }

  public void shouldSeeRegisterButton() {
    getRegisterButton().isDisplayed();
  }


  public void setFirstName(String value) {
    InputHelper.setInputValue(getFirstNameInput(), value, "First name field is not displayed");
    account.setFirstname(value);
  }

  public void setLastName(String value) {
    InputHelper.setInputValue(getLastNameInput(), value, "Last name field is not displayed");
    account.setLastname(value);
  }

  public void setBirthDay() {
    getBirthDayInput().click();
    List<WebElement> date = driver.findElements(By.className("android.widget.NumberPicker"));
    date.get(0).sendKeys("12");
    date.get(1).sendKeys("Apr");
    date.get(2).sendKeys("1992");
    date.get(2).click();
    driver.findElementByName("Salvesta").click();
    account.setBirthday("1992-04-12");
  }

  public void setFirstEmail(String value) {
    InputHelper.setInputValue(getFirstEmailInput(), value, "First email field is not displayed");
    account.setEmail(value);
  }

  public void setSecondEmail(String value) {
    InputHelper.setInputValue(getSecondEmailInput(), value, "Second email field is not displayed");
    account.setEmailVerification(value);
  }

  public void setFirstPassword(String value) {
    InputHelper.setInputValue(getFirstPasswordInput(), value, "First Password field is not displayed");
    account.setPassword(value);
  }

  public void setSecondPassword(String value) {
    InputHelper.setInputValue(getSecondPasswordInput(), value, "Second Password field is not displayed");
    account.setPasswordVerifivation(value);
  }

  public void pressRegister() {
    shouldSeeRegisterButton();
    getRegisterButton().click();
  }

  public void shouldSeeAlertWithText(String text) {
    AlertHelper.shouldHaveAlert(text, "Expected alert with content: " + text + " do not exist");
  }

  public String generateUniqueEmailAddress() {
    return generateRandomString()+"@"+generateRandomString()+".ee";
  }

  private String generateRandomString() {
    SecureRandom random = new SecureRandom();
    return new BigInteger(250, random).toString(32);
  }

  public void saveAccount() throws IOException {
    JSONFileHandler generator = new JSONFileHandler();
    generator.saveToFile(account);
  }

  public void registerWithValidInformation(){
    setFirstName("Marko");
    setLastName("Konsa");
    setBirthDay();
    setFirstEmail(generateUniqueEmailAddress());
    setSecondEmail(account.getEmail());
    setFirstPassword("TestCase123");
    setSecondPassword("TestCase123");
  }

  public void enterAlreadyRegisteredAccountDetailsToRegisteringForm() throws IOException, ParseException {
    JSONFileHandler handler = new JSONFileHandler();
    Account validAccount = handler.loadAccountFromJson();
    setFirstName(validAccount.getFirstname());
    setLastName(validAccount.getLastname());
    setBirthDay();
    setFirstEmail(validAccount.getEmail());
    setSecondEmail(validAccount.getEmailVerification());
    setFirstPassword(validAccount.getPassword());
    setSecondPassword(validAccount.getPasswordVerifivation());
  }

  public void shouldNotBeAlert() {
    if (AlertHelper.isAlert()){
      throw new AssertionError("There is alert with text: "+AlertHelper.getAlertError());
    }
  }
}
