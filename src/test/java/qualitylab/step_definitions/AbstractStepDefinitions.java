package qualitylab.step_definitions;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class AbstractStepDefinitions {

  protected static AndroidDriver driver;
  protected static WebDriverWait wait;

  public static AndroidDriver getAndroidDriver() throws MalformedURLException {
    if (driver == null) {
      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
      capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "4.4");
      capabilities.setCapability(MobileCapabilityType.APP, "<test apk path here>");
      capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Emulator");
      driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
      wait = new WebDriverWait(driver, 30);
    }
    return driver;
  }
}
