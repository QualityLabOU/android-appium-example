package qualitylab.step_definitions;

import qualitylab.activities.LoginActivity;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.MalformedURLException;

public class LoginStepDefinitions extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;
  LoginActivity loginActivity;

  @Before
  public void setup(Scenario scenario) throws MalformedURLException{
    driver = getAndroidDriver();
    this.scenario = scenario;
    this.loginActivity = new LoginActivity(scenario);
  }

  @Given("^I am on the login page$")
  public void i_am_on_the_login_page() {
    loginActivity.shoudSeeLoginButton();
  }

  @When("^I navigate to registering activity$")
  public void I_navigate_to_registering_activity() {
    loginActivity.clickRegisterNewUser();
  }

  @When("^I login with valid credentials$")
  public void I_login_with_valid_credentials() throws IOException, ParseException {
    loginActivity.loginWithValidCredentials();
  }

  @When("^I login with invalid credentials$")
  public void I_login_with_invalid_credentials() {
    loginActivity.loginWithInvalidCredentials();
  }

  @When("^I try to log in with old password$")
  public void I_try_to_log_in_with_old_password() throws IOException, ParseException {
    loginActivity.loginWithOldPassword();
  }

  @When("^I login with valid email and \"([^\"]*)\"$")
  public void I_login_with_valid_email_and(String password) throws IOException, ParseException {
   loginActivity.loginWithValidEmailAnd(password);
  }
}
