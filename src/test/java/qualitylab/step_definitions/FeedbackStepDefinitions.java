package qualitylab.step_definitions;

import qualitylab.activities.FeedbackActivity;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;

public class FeedbackStepDefinitions extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;
  FeedbackActivity feedbackActivity;

  @Before
  public void setup(Scenario scenario) throws MalformedURLException {
    driver = getAndroidDriver();
    this.scenario = scenario;
    this.feedbackActivity = new FeedbackActivity(scenario);
  }
  @Then("^I should see feedback button$")
  public void I_should_see_feedback_button() {
    feedbackActivity.shouldSeeFeedbackButton();
  }

  @When("^I click on feedback button$")
  public void I_click_on_feedback_button() {
    feedbackActivity.clickOnFeedbackButton();
  }

  @Then("^I should be on the feedback activity$")
  public void I_should_be_on_the_feedback_activity() {
    feedbackActivity.shouldBeOnTheFeedbackActivity();
  }

  @When("^I choose \"([^\"]*)\" from dropdown$")
  public void I_choose_from_dropdown(String place) {
    feedbackActivity.chooseFromDropDown(place);
  }

  @And("^choose number \"([^\"]*)\" for feedback$")
  public void choose_number_for_feedback(String feedback){
    feedbackActivity.chooseFeedbackNumber(feedback);
  }

  @And("^press send feedback$")
  public void press_send_feedback() {
    feedbackActivity.pressSendFeedback();
  }

  @Then("^I should no see alert$")
  public void I_should_no_see_alert(){
    feedbackActivity.shouldNotSeeAlert();
  }
}
