package qualitylab.step_definitions;

import qualitylab.activities.MenuActivity;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;

public class MenuActivityStepDefinitions extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;
  MenuActivity menuActivity;

  @Before
  public void setup(Scenario scenario) throws MalformedURLException {
    menuActivity = new MenuActivity(scenario);
    driver = getAndroidDriver();
    this.scenario = scenario;
  }

  @Then("^I should be logged in$")
  public void I_should_be_logged_in() {
    menuActivity.shouldBeLoggedIn();
  }

  @When("^I open the sidemenu$")
  public void I_open_the_sidemenu() {
    menuActivity.openSideMenu();
  }

  @Then("^I should see log out button$")
  public void I_should_see_log_out_button() {
    menuActivity.shoudSeeLogoutButton();
  }

  @When("^I log out$")
  public void I_log_out() {
    menuActivity.pressLogout();
  }
}
