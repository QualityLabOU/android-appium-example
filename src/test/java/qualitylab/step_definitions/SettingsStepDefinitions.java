package qualitylab.step_definitions;

import qualitylab.activities.SettingsActivity;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.MalformedURLException;

public class SettingsStepDefinitions extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;
  SettingsActivity settingsActivity;

  @Before
  public void setup(Scenario scenario) throws MalformedURLException {
    driver = getAndroidDriver();
    this.scenario = scenario;
    this.settingsActivity = new SettingsActivity(scenario);
  }

  @Then("^I should see settings button$")
  public void I_should_see_log_out_button() {
    settingsActivity.shouldSeeSettingsButton();
  }

  @When("^I click settings button$")
  public void I_click_settings_button() {
    settingsActivity.clickSettingsButton();
  }

  @Then("^I should be on the my settings activity$")
  public void I_should_be_on_the_my_settings_activity() {
    settingsActivity.shouldBeOnTheSettingsActivity();
  }

  @And("^I check account details$")
  public void I_check_account_details() throws IOException, ParseException {
    settingsActivity.checkAccountDetails();
  }

  @And("^I change my password to \"([^\"]*)\"$")
  public void I_change_my_password_to(String password) {
    settingsActivity.changePasswordTo(password);
  }

}
