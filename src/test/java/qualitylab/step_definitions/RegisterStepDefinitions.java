package qualitylab.step_definitions;

import qualitylab.activities.RegisterActivity;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.MalformedURLException;

public class RegisterStepDefinitions extends AbstractStepDefinitions {

  Scenario scenario;
  AndroidDriver driver;
  RegisterActivity registerActivity;

  @Before
  public void setup(Scenario scenario) throws MalformedURLException {
    registerActivity = new RegisterActivity(scenario);
    driver = getAndroidDriver();
    this.scenario = scenario;
  }

  @Then("^I should see register button$")
  public void i_should_see_register_button() {
    registerActivity.shouldSeeRegisterButton();
  }
  @When("^I type \"(.*?)\" into the first name$")
  public void i_type_into_the_first_name(String name) {
    registerActivity.setFirstName(name);
  }

  @When("^I type \"(.*?)\" into the last name$")
  public void i_type_into_the_last_name(String name){
    registerActivity.setLastName(name);
  }

  @When("^I type \"(.*?)\" into the email$")
  public void i_type_into_the_email(String email){
    registerActivity.setFirstEmail(email);
  }

  @When("^I type \"(.*?)\" into the email again$")
  public void i_type_into_the_email_again(String email){
    registerActivity.setSecondEmail(email);
  }

  @When("^I type \"(.*?)\" into the password$")
  public void i_type_into_the_password(String pass){
    registerActivity.setFirstPassword(pass);
  }

  @When("^I type \"(.*?)\" into the password again$")
  public void i_type_into_the_password_again(String pass){
    registerActivity.setSecondPassword(pass);
  }

  @When("^I press register button$")
  public void i_press_register_button(){
    registerActivity.pressRegister();
  }

  @Then("^I should see alert with text \"([^\"]*)\"$")
  public void I_should_see_alert_with_text(String text) {
   registerActivity.shouldSeeAlertWithText(text);
  }

  @And("^I enter the birthday$")
  public void I_enter_set_the_birthday() {
    registerActivity.setBirthDay();
  }

  @And("^I enter unique email address$")
  public void I_enter_unique_email_address(){
    registerActivity.setFirstEmail(registerActivity.generateUniqueEmailAddress());
  }

  @And("^I repeat unique email address$")
  public void I_repeat_unique_email_address() {
    registerActivity.setSecondEmail(RegisterActivity.account.getEmail());
  }

  @And("^save lastly created user$")
  public void save_lastly_created_user_email() throws IOException {
    registerActivity.saveAccount();
  }

  @When("^I enter valid account details to registering form$")
  public void I_enter_valid_account_details_to_registering_form() throws IOException, ParseException {
    registerActivity.enterAlreadyRegisteredAccountDetailsToRegisteringForm();
  }

  @And("^I register new valid account$")
  public void I_register_new_valid_account() throws IOException, ParseException {
    registerActivity.enterAlreadyRegisteredAccountDetailsToRegisteringForm();
    registerActivity.pressRegister();
    registerActivity.shouldNotBeAlert();
  }
}


